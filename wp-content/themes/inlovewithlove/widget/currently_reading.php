<?php
/**
 * Created by PhpStorm.
 * User: Ben
 * Date: 27/09/2015
 * Time: 16:55
 */

class WP_Widget_Currently_Reading extends WP_Widget {

    function __construct() {
        $widget_ops = array('classname' => 'currently_reading_widget', 'description' => __( "The book you're reading at the moment") );
        parent::__construct('currently_reading_widget', __('Currently Reading'), $widget_ops);
        $this->alt_option_name = 'wp_currently_reading_widget';

        add_action( 'save_post', array($this, 'flush_widget_cache') );
        add_action( 'deleted_post', array($this, 'flush_widget_cache') );
        add_action( 'switch_theme', array($this, 'flush_widget_cache') );
    }

    function widget($args, $instance) {
        $cache = wp_cache_get('currently_reading_widget', 'widget');

        if ( !is_array($cache) )
            $cache = array();

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        ob_start();
        extract($args);

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Currently Reading' );
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        $the_book = ( ! empty( $instance['the_book'] ) ) ? absint( $instance['the_book'] ) : 'Nothing right now'; ?>

        <?php echo $before_widget; ?>
        <?php if ( $title ) echo $before_title . $title . $after_title; ?>
        <div class="col-xs-12 currently-reading">
            <div class="row" id="book_title">
                <?php
                    $curr_read = get_post($the_book);
                    echo $curr_read->post_title;
                ?>
            </div>
            <div class="row" id="book_by">
                By
            </div>
            <div class="row" id="book_author">
                <?php
                    echo get_field("author", $curr_read)->name;
                ?>
            </div>
        </div>
        <?php echo $after_widget;

        $cache[$args['widget_id']] = ob_get_flush();
        wp_cache_set('currently_reading_widget', $cache, 'widget');
    }

    function update( $new_instance, $old_instance ) {
        var_dump($new_instance);
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['the_book'] = $new_instance['the_book'];

        $this->flush_widget_cache();

        return $instance;
    }

    function flush_widget_cache() {
        wp_cache_delete('currently_reading_widget', 'widget');
    }

    function form( $instance ) {
        #TODO: Fix the dropdown resetting on 'Save'
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $the_book  = isset( $instance['the_book'] ) ? $instance['the_book'] : 'None selected';
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <?php
        $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'post_type' => 'book', 'posts_per_page' => 10, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
        if ($r->have_posts()) : ?>
            <p><label for="<?php echo $this->get_field_id( 'the_book' ); ?>"><?php _e( 'Book:' ); ?></label>
                <select class="widefat" id="<?php echo $this->get_field_id( 'the_book' ); ?>" name="<?php echo $this->get_field_name( 'the_book' ); ?>">
                <?php while ( $r->have_posts() ) : $r->the_post();
                    if (intval($the_book) == intval(the_ID())) {?>
                        <option value="<?php the_ID() ?>" selected="selected"><?php the_title() ?></option>
                    <?php } else { ?>
                        <option value="<?php the_ID() ?>"><?php the_title() ?></option>
                <?php } endwhile; ?>
                </select>
            </p>
            <?php wp_reset_postdata();
        endif;
    }
}