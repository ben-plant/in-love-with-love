<?php

require_once('wp_bootstrap_navwalker.php');
require_once('widget/new_books.php');
require_once('widget/currently_reading.php');

function load_fonts() {
	wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Satisfy|Quicksand|Cabin:400,500,600,700|Amatic+SC');
	wp_register_style('fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
    wp_enqueue_style( 'googleFonts');
    wp_enqueue_style( 'fontAwesome');
}
add_action('wp_print_styles', 'load_fonts');

function my_acf_update_value( $post_id ) {
	global $_POST;
	$post_type_book = 'book';
	if ($post_type_book == get_post($post_id)->post_type) {
		$new_title = get_field('book_title', $post_id);
		$new_slug = sanitize_title( $new_title );

		$my_post = array(
	    	'ID'           => $post_id,
	        'post_title' => $new_title,
		    'post_name' => $new_slug
	  	);
		remove_action( 'save_post', 'my_acf_update_value' );
	  	wp_update_post( $my_post );
		add_action( 'save_post', 'my_acf_update_value' );
	}
}
add_action( 'save_post', 'my_acf_update_value' );

function load_custom_widgets() {
	register_widget('WP_Widget_New_Books');
	register_widget('WP_Widget_Currently_Reading');
}
add_action( 'widgets_init', 'load_custom_widgets');
?>