<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title( '|', true, 'right' ); ?></title>	
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		<!-- IE8 fallback moved below head to work properly. Added respond as well. Tested to work. -->
			<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->	
		
			<!-- respond.js -->
		<!--[if lt IE 9]>
		          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
		<![endif]-->	
	</head>
	
	<body <?php body_class(); ?>>

		<div class="container main-wrapper">
			<div class="row header-wrapper">
				<div class="container">
					<div id="social-row" class="row">
						<div class="col-xs-6">
							<?php if (!is_front_page() || !is_home()) { ?>
								<div class="home-wrapper">
									<ul id="home-icon">
										<li class="social-icon"><a href="/"><i class="fa fa-home"></i></a></li>
									</ul>
								</div>
							<?php } ?>
						</div>
						<div class="col-xs-6">
							<div class="social-wrapper">
								<ul id="social-list">
									<li class="social-icon"><i class="fa fa-facebook"></i></li>
									<li class="social-icon"><i class="fa fa-twitter"></i></li>
									<li class="social-icon"><i class="fa fa-pinterest"></i></li>
									<li class="social-icon"><i class="fa fa-envelope"></i></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row main-title-wrapper">
						<div class="title-wrapper" id="main-title-wrapper">
							<?php echo bloginfo('name'); ?>
						</div>
						<div class="title-wrapper" id="main-subtitle-wrapper">
							Abbie's Book Blog
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php
					wp_nav_menu( array(
						'menu' => 'top_menu',
						'depth' => 10,
						'container' => false,
						'menu_class' => 'nav',
						//Process nav menu using our custom nav walker
						'walker' => new wp_bootstrap_navwalker())
					);
				?>
			</div>
			<?php if (is_front_page()) { ?>
				<div class="row">
					<div class="anim-bookshelf">
						<?php $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'post_type' => 'book', 'posts_per_page' => 4, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
						if ($r->have_posts()) :
							while ( $r->have_posts() ) : $r->the_post(); ?>
								<div class="col-xs-3 slider-book-column">
									<div class="book slider-book">
									<?php $image = get_field('cover_photo');
									if (!empty($image)) { ?>
										<a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID() ); ?>">
											<img class="slider-book-cover" src="<?php echo $image['url'] ?>">
											<?php if (strtotime(get_the_date()) >= strtotime('48 hours ago')) { ?>
												<img class="new-sash big-sash" src="<?php echo get_stylesheet_directory_uri(); ?>/img/new.png">
											<?php } ?>
										</a>
									<?php }
									?>
									</div>
								</div>
							<?php endwhile;
						wp_reset_postdata();
						endif;
						?>
					</div>
				</div>
			<?php } ?>
			<div class="row">
				<div class="col-xs-12">
					<div class="container">
